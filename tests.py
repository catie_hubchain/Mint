#!/usr/bin/env python3

import os
import json


class Test:
    def __init__(self, str_args, **kwargs):
        if str_args:
            name, path, codes = str_args.split(" ", 2)
            codes = map(int, codes.split(" "))
        else:
            name, path, codes = kwargs["name"], kwargs["path"], kwargs["codes"]
        self.name = name
        self.path = os.getcwd() + "/contract_tests/" + path
        self.codes = list(codes)
    
    def run(self):
        os.system("python3 main.py --path {}".format(self.path))
        with open("results/security.log", "r") as f:
            content = f.read()
            if content:
                logs = json.loads(content)
            else:
                logs = []
        logs_codes = [log["code"] for log in logs]
        for code in self.codes:
            if self.codes.count(code) != logs_codes.count(code):
                print ("Test {} did not pass !".format(self.name))
                return False
        
        print ("Test {} passed !".format(self.name))
        return True
            


solidity_files = [
    '1 greedy.sol 3', 
    '2 unsafe_calls.sol 4', 
    '3 too_much_gas_on_fallback.sol 6', 
    '4 arbitrary_calls.sol 2 5', 
    '5 prodigal.sol 2 2 2', 
    '6 overunderflow.sol 2 7', 
    '7 suicidable.sol 1'
    ]

tests = [[Test(sol_file), None] for sol_file in solidity_files]

for i, (test, _) in enumerate(tests):
    tests[i][1] = test.run()

print ("=== TESTS SUMMARY ===")
for (test, result) in tests:
    if result:
        print ("Test {} passed !".format(test.name))
    else:
        print ("Test {} did not pass !".format(test.name))