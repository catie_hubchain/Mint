#!/usr/bin/env python

"""
Tool for analyzing Solidity smart-contracts.

This tool builds execution trees of a smart-contract and highlight its 
vulnerabilities.

args:   --path: path of the smart-contract
        --view: enable tree views building
"""

import os
import sys

import solc

from sources import translater, tree_builder, security, view
from sources.utils import ArgParser

DEBUG = False


def clean_results():
    """
    remove last results from `result` directory
    """
    if os.listdir("results/"):
        os.system("rm -r results/*")


if __name__ == "__main__":

    # argument parser
    arg_parser = ArgParser("p path 1", "vi view 0")
    arg_dict = arg_parser.run()

    view_arg = "view" in arg_dict
    contract_path = arg_dict["path"][0] if "path" in arg_dict else ""

    # clear result's directory
    clean_results()
    f = open("results/security.log", "w")
    f.close()

    # count how many security warnings are raised
    nb_alerts = 0

    # contract path
    if DEBUG:
        contract_path = "~/espaces/SmartContractsSecurity/ProjectTest/contracts/Test.sol"
    elif not contract_path :
        contract_path = input("Please give the contract fullpath : ")
    
    # compile and get bytecode
    # with open(contract_path, "r") as f:
    #     compiled = solc.compile_source(f.read())
    compiled = solc.compile_files([contract_path])
    for k, v in compiled.items():
        contract_name = k.rsplit("/")[-1].split(":")[1]
        bc = v["bin-runtime"]
        if not bc:
            continue
        abi = v["abi"]
        print ("\n\n===== Contract {} =====\n".format(contract_name))
        
        # creates a new directory for storing the contract's trees and json
        os.mkdir("results/{}".format(contract_name))
        os.mkdir("results/{}/img".format(contract_name))
        os.mkdir("results/{}/json".format(contract_name)) # no json stored anymore but still, i prefer not removing it for now
        
        # run translater.py
        translater.main(bc)
        # run tree_builder.py
        tree_functions = tree_builder.main(contract_name, abi)
        # run view.py if argument `view` is given
        if view_arg :
            view.main(contract_name, tree_functions)
        # run security.py
        nb_alerts += security.main(contract_name, tree_functions)

    print ("\n\n===== Summary =====\n")
    print ("Execution ended with {} security alert(s), see ./results/security.log for more information".format(
                nb_alerts
            ))
