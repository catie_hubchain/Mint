pragma solidity ^0.4.4;


contract ArbitraryCalls {

    address owner;

    function unsafe(address _to) public {
        require(_to.call());
    }

    function safe1(address _to) public {
        require(_to == owner);
        require(_to.call());
    }

    function safe2(address _to) public {
        require(msg.sender == owner);
        require(_to.call());
    }
}