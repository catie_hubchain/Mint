pragma solidity ^0.4.4;


contract UncheckedCalls {

    address owner;

    function checked_call() public {
        require(msg.sender == owner);
        require(msg.sender.call());
    }

    function unchecked_call() public {
        require(msg.sender == owner);
        msg.sender.call();
    }
}