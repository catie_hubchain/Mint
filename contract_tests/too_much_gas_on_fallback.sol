pragma solidity ^0.4.4;


contract BigFallback {

    address public owner;

    function () {
        for (uint i=0; i<20; i++) {
            uint x = 3;
        }
    }

    function killme(){
        require(msg.sender == owner);
        selfdestruct(owner);
    }
}