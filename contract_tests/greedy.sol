pragma solidity ^0.4.4;


contract Greedy {

    uint balance;

    function give() public payable {
        balance += msg.value;
    }
}