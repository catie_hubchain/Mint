pragma solidity ^0.4.4;


contract Prodigal {


    address public owner;
    address public target;

    function Prodigal() public {
        owner = msg.sender;
    }

    function safe_give1(address _to, uint amount) public {
        require(owner == msg.sender);
        msg.sender.transfer(amount);
    }

    function safe_give2(address _to, uint amount) public {
        require(owner == msg.sender);
        _to.transfer(amount);
    }

    function safe_give3(address _to, uint amount) public {
        require(amount == 0);
        _to.transfer(amount);
    }

    function vulnerable_give1(address _to, uint amount) public {
        _to.transfer(amount);
    }

    function vulnerable_give2(uint amount) public {
        msg.sender.transfer(amount);
    }

    function vulnerable_give3(uint amount) public {
        target.transfer(amount);
    }
}