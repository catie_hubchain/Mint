pragma solidity ^0.4.4;


contract Overflow {

    mapping(address=>uint) public balances;

    function get_balance() public returns (uint) {
        return balances[msg.sender];
    }

    function give(address _to) public payable {
        balances[_to] += msg.value;
    }

    function withdraw(uint _amount) public {
        require(balances[msg.sender] >= _amount);
        msg.sender.transfer(_amount);
        balances[msg.sender] -= _amount;
    }

    function batchTransfer(address[] receivers, uint _amount) public {
        uint total = receivers.length * _amount;
        require(balances[msg.sender] >= total);
        balances[msg.sender] -= total;
        for (uint i = 0; i < receivers.length; i++) {
            balances[receivers[i]] += _amount;
        }
    }
}