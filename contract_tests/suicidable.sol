pragma solidity ^0.4.4;


contract Suicidable {


    address public owner;

    function Suicidable() public {
        owner = msg.sender;
    }

    function safe_killme() public {
        require(owner == msg.sender);
        selfdestruct(owner);
    }

    function vulnerable_killme() public {
        selfdestruct(owner);
    }
}