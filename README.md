# MINT

Mint est un outil d'analyse et de détection de vulnérabilités de Smart Contrats écrits dans le langage solidity.  
Il utilise des techniques d'analyse dynamique et de «pattern matching» pour détecter plusieurs sortes de vulnérabilités (décrites ci-dessous).  


# DOCUMENTATION

La documentation de chaque fonction utilisée dans l'outil peut être trouvée dans le répertoire ```Doc```.  
Pour consulter la documentation complète, il suffit d'ouvrir ```sources.html``` dans un navigateur.

--------------------------------
## IMPORTS NÉCESSAIRES

- pysha3
- pyyaml
- numpy
- recordclass
- solc (py-solc version 2.1.0, solc binary version 0.4.24)

--------------------------------
## ANALYSE

Pour lancer l'analyse, ouvrez un terminal dans le répertoire ```Mint``` et tapez:

```console
python3 main.py
```
Il faudra ensuite entrer le chemin d'accès du contrat à analyser (le fichier .sol).  
```console
python3 main.py
Please give the contract path : ~/.../contracts/Test.sol
```

Vous pouvez aussi entrer le chemin d'accès en argument en utilisant l'option --path (ou -p):
```console
python3 main.py --path ~/.../contracts/Test.sol
```

- **Attention**, les constructeurs des contrats ne sont pas analysés par cet outil.
--------------------------------
## COMPILER

La partie compilation compile le contrat et récupère le bytecode.


--------------------------------
## TRANSLATER

La module ```translater``` traduit le bytecode en une suite d'opcodes.  
Il stocke ces opcodes dans le répertoire```./results/```, dans un fichier json qui sera ensuite lu par le programme, et dans un fichier texte destiné à être lu par un humain.  


--------------------------------
## TREE BUILDER

Le module ```tree_builder``` construit les arbres d'exécution de chaque fonction du contrat.  
Durant l'exécution de ce module, des avertissements peuvent s'afficher:
- **Bounding Warning** : cet avertissement signifie que certaines boucles dans le contrat ont dû être bornées pour éviter une exécution infinie. Par conséquent, l'analyse de la fonction concernée peut ne pas être complète.


--------------------------------
## VIEW

Le module ```view``` dessine les arbres et stocke les dessins dans le répertoire ```./results/img/```.  
Pour lancer ce module, l'option --view doit être ajoutée.
```console
python3 main.py --path ~/espaces/SmartContractsSecurity/ProjectTest/contracts/Test.sol --view
```
Notez que certains arbres peuvent ne pas être dessinés car trop larges...


--------------------------------
## SECURITY

Le module ```security``` recherche des vulnérabilités dans le contrat, pour le moment, les vulnérabilités prise en compte sont:

- suicidal faults : un contrat est qualifié de suicidal s'il peut être détruit par n'importe qui [1]
- prodigal faults : un contrat est qualifié de prodigal s'il peut transférer de l'ether à n'importe qui [2]
- greedy fault : un contrat est qualifié de greedy s'il peut recevoir de l'ether mais n'a aucun moyen d'en redonner [3]
- check on CALL : vérifie si les valeurs retournées par les fonctions CALL sont testées [4]
- call on arbitrary addresses: vérifie si contrat peut appeler une adresse arbitraire [5]
- function fallback too big : une fonction fallback ne doit pas consommer plus de 2300 unités de gaz [6]
- underflow, overflow attacks : les opérations sur des variables uintX sont calculées modulo 2^^X^^ [7]

Les avertissements affichés durant l'analyse sont stockés dans le répertoire ```./results/security.log```, les codes associés à un avertissement correspondent aux nombres entre crochets ci-dessus.


--------------------------------
## Fonctionnement des tests du module security

### Test suicidable
Le but est de tester si la fonction en entrée est suicidal, ie si elle peut être tuée par n'importe qui.  
- On recherche les instructions `SELFDESTRUCT` dans les noeuds de l'arbre associé à cette fonction.  
- Quand on en trouve un, on inspecte les contraintes qui conduisent à ce noeud  
- Si le msg.sender (`CALLER`) n'est impliqué dans aucune de ces contraintes, cela veut dire que n'importe qui peut arriver à ce noeud, ie tuer la fonction.  



### Test prodigal
Le but est de tester si la fonction en entrée est prodigal, ie si elle peut envoyer de l'ether sans restriction sur l'utilisateur qui l'appelle.  
- On recherche les instructions `CALL` dont le paramètre `value` est non nul dans les noeuds de l'arbre associé à cette fonction.
- Quand on en trouve un, on inspecte les contraintes qui conduisent à ce noeud
- Si le msg.sender (`CALLER`) n'est impliqué dans aucune de ces contraintes, cela veut dire que n'importe qui peut arriver à ce noeud, ie tuer la fonction.  



### Test greedy
Le but est de tester si le contrat est greedy, ie s'il peut recevoir de l'ether mais pas en envoyer (et donc qu'il ne peut pas être tué : l'instruction `suicide` prend en paramètre une address à qui envoyer l'ether stockés dans le contrat).  

La fonction `function_is_payable` retourne si la fonction en entrée peut recevoir de l'ether (tag `payable` dans Solidity) ou non.
Pour cela, elle vérifie s'il existe un noeud dans lequel:
- il n'y a pas d'instruction `REVERT` (qui annule tout échange d'ether effectué et stop l'exécution de la fonction)
- il n'y a pas dans les contraintes menant à ce noeud un test `VALUE == 0`
Si ces deux conditions sont vérifiées, elle retourne True: la fonction peut envoyer de l'ether.

La fonction `function_can_send_ether` retourne si la fonction peut envoyer de l'ether ou non.
Pour cela elle vérifie s'il existe un noeud dans lequel il y a des instructions suivantes :
- `SELFDESTRUCT`ou `SUICIDE` : tuer un contrat envoi son ether à une address
- `CALL` avec un paramètre `value` non nul
Si c'est le cas elle retourne True.

Finalement, une fonction est considérée comme greedy si elle est `payable` et qu'elle ne peut pas envoyer de l'ether.



### Test uncheckedcalls
Le but est de tester si des instructions `CALL` sont effectuées sans test sur la valeur retourné.  
*Sur solidity, les fonctions `send` et `transfer` effectuent naturellement un test sur cette valeur et ne posent donc pas de problème.
Cependant, la fonction `call` n'en effectue pas, il faut le faire manuellement.*  

- la fonction `check_on_call` vérifie, pour chaque instruction `CALL` de chaque noeud de la fonction en entrée, si le test qui suit le `CALL` est bien `ISZERO(<callReturnedValue>)`.



### Test call on arbitrary addresses
Le but est de tester si le contrat effectue des appels d'addresses inconnues.  
- On récupère l'adresse cible de chaque instruction `CALL` de chaque noeud
- On vérifie si l'adresse est soumise à un test dans les contraintes conduisant au noeud correspondant



### Test too much gas on fallback
Le but est de vérifier que la fonction fallback d'un contrat ne consomme pas plus de 2300 unités de gaz.  
C'est important car les fonctions `transfer` et `send` de solidity effectue un `CALL` avec seulement 2300 gaz pour éviter les attaques par réentrances.



### Test underflows / overflows
Le but est de vérifier si des tests sont effectués sur des variables pouvant être attaquées par underflow ou overflow.  
La fonction `operations_overflow_underflow` dresse la liste de toutes les opérations `ADD`, `MUL`, `SUB` et `EXP` de la fonction en entrée.  
Puis elle retourne la liste des opérations de cette liste effectuées sur des variables *unsafe*, ie qui peuvent être modifiées par un utilisateur.

La fonction `overflow_underflow` récupère la liste d'opérations sensibles retournée par `operations_overflow_underflow` et vérifie si ces opérations sont utilisées pour des tests (`JUMPIF`), si c'est le cas, le contrat est vulnérable car un utilisateur lambda peut modifier ces le résultat de ces opérations avec une attaque en overflow / underflow et peut ainsi accéder à des noeuds du contrats auxquels il ne devrait pas avoir accès.