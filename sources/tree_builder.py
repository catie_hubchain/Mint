#
# Build the tree of the EVM execution
#


# IMPORTS

import copy  # for deepcopys
import json
import os
import sys
from collections import deque  # for stack
from json import JSONDecoder, JSONEncoder

import yaml  # for config file
from recordclass import recordclass  # struct datatype
from sha3 import keccak_256

from sources import symbolic_execution, SymbolicClasses


# CONFIG - Loads variables from config.yml
with open("sources/config.yml", "r") as f:
    for k, v in yaml.safe_load(f)["tree_builder"].items():
        vars()[k] = v


def func_signatures(abi):
    """
    Computes the signature of the functions of abi
    Returns a dict which maps names to signatures
    """

    mapping = {}

    for f_abi in abi:
        # if "normal" function
        if "name" in f_abi:
            prototype = f_abi["name"] + "("
            types = [ipt["type"] for ipt in f_abi["inputs"]]
            prototype += ",".join(types) + ")"
            hex_sig = keccak_256(prototype.encode()).hexdigest()[:8]
            mapping[int(hex_sig, 16)] = f_abi["name"]
        # if fallback or constructor function
        else:
            pass

    return mapping


class MyEncoder(JSONEncoder):
    """
    For serialyzation of Expressions and BinaryTrees
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, indent=True)

    @staticmethod
    def default(obj):
        if isinstance(obj, SymbolicClasses.Expression):
            slots = obj.__slots__
            ser = {slots[i]: getattr(obj, slots[i]) for i in range(len(slots))}
            ser["Expression"] = type(obj).__name__
            return ser

        elif isinstance(obj, BinaryTree):
            slots = obj.__slots__
            ser = {slots[i]: getattr(obj, slots[i]) for i in range(len(slots))}
            ser["Tree"] = True
            return ser

        else:
            raise TypeError(type(obj).__name__, str(obj))


class MyDecoder(JSONDecoder):
    """
    For serialyzation of Expressions and BinaryTrees
    """

    @staticmethod
    def from_json_obj(json_object):
        if "Expression" in json_object:
            obj = vars(SymbolicClasses)[json_object["Expression"]]
            del json_object["Expression"]
            return obj(**json_object)

        elif "Tree" in json_object:
            del json_object["Tree"]

            return BinaryTree(json_object["left_son"],
                              json_object["right_son"],
                              **json_object["node"])

        else:
            return json_object

    def __init__(self):
        super().__init__(object_hook=self.from_json_obj)


class BinaryTree:
    """
    Class for symbolic execution's tree representation
    """

    __slots__ = ("node", "left_son", "right_son")  # attributes of the class

    def __init__(self, left_son=None, right_son=None, **kwargs):
        self.node = kwargs
        self.left_son = left_son
        self.right_son = right_son

    def fill_node(self, current):
        """
        fills the node of `self`
        """

        for attr in ("path", "variables", "condition",
                     "path_constraints", "gas", "block"):
            self.node[attr] = getattr(current, attr)
    
    def clean_fallback(self, function_trees) :
        """
        Remove function_trees branchs of `self` (`self` must be the fallback function) 
        """

        trees_left = deque([self])

        while trees_left :
            current = trees_left.pop()

            if current.left_son in function_trees :
                current.left_son = BinaryTree()
            if current.right_son in function_trees :
                current.right_son = BinaryTree()

            if current.left_son:
                trees_left.append(current.left_son)
            if current.right_son:
                trees_left.append(current.right_son)


    @classmethod
    def from_opcodes(cls, opcodes_json, mapping):
        """
        Iteratively builds a list of trees, one for each function

        `opcodes_json`: a json file with opcodes
        `mapping`: the dict from `func_signatures`
        """

        DEBUG = False  # debug mode : to see each operations

        fully_symbolic_tests_pc = {}  # in order to handle infinite loops
        boundings_pc = []  # position (prog counter) of the boundings

        Current = recordclass(
            "Current", 
            "tree root state block index path gas condition"
                        " path_constraints variables")

        fallback = BinaryTree(name="fallback")

        trees = [fallback]  # the list of trees which will be returned
        
        current = Current(
            tree=fallback,
            root=None,
            state=symbolic_execution.State(),
            block=[],
            index=0,
            path="",
            gas=0,
            condition=None,
            path_constraints=[],
            variables=[]
        )

        forks = deque()
        while True:

            line = opcodes_json[current.index]
            current.gas += line["gas"]
            ret = current.state.next_line(line)
            line.update(ret)

            # handling symbolic loops
            if isinstance(ret["output"], SymbolicClasses.Test):
                if not isinstance(ret["output"].left, SymbolicClasses.Concret) \
                        or not isinstance(ret["output"].right, SymbolicClasses.Concret):
                    if line["program_counter"] not in fully_symbolic_tests_pc:
                        fully_symbolic_tests_pc[line["program_counter"]] = 1
                    else:
                        fully_symbolic_tests_pc[line["program_counter"]] += 1

                    # if the test has been visited too often
                    if fully_symbolic_tests_pc[line["program_counter"]] >= MAX_LOOP_ITERATIONS:

                        if fully_symbolic_tests_pc[line["program_counter"]] == MAX_LOOP_ITERATIONS:
                            boundings_pc.append(line["program_counter"])
                            # set the test to True
                            current.state.stack[-1] = SymbolicClasses.Concret(
                                1)
                            fully_symbolic_tests_pc[line["program_counter"]] += 1

                        # if true is not the exit condition, set the test to False
                        elif fully_symbolic_tests_pc[line["program_counter"]] > MAX_LOOP_ITERATIONS:
                            current.state.stack[-1] = SymbolicClasses.Concret(
                                0)

                        current.root.node["is_bounded"] = True

            current.block.append(line)
            current.gas += line["gas"]
            current.index += 1

            if DEBUG:
                print("-------------------------------------------------------")
                print("[%d] %s %s\n" %
                      (line["program_counter"], line["name"], line["args"]))
                print("\n".join(list(map(str, current.state.stack))[::-1]))
                input()

            # instruction that stops the execution
            if line["name"] in ("STOP", "REVERT", "RETURN", "INVALIDOPERATION"):

                current.tree.fill_node(current)

                if not forks:
                    if boundings_pc:
                        print(
                            "\n-- Warning : some loops had to be bounded"
                            " - program counters {}".format(boundings_pc))
                    
                    fallback.clean_fallback(trees)
                    return trees

                else:
                    current = forks.pop()
                    if current.tree in trees:
                        current.root = current.tree

            # jumpi instruction
            elif line["name"] == "JUMPI":

                current.tree.fill_node(current)
                current.tree.left_son = BinaryTree()
                current.tree.right_son = BinaryTree()

                address_jump, condition = ret["input"]

                # in the case where the condition is symbolic
                if not isinstance(condition, SymbolicClasses.Concret):

                    index_jump_destination = [j for j in range(
                        len(opcodes_json)) if opcodes_json[j]["program_counter"] == address_jump.value][0]

                    # checks if it is the start of a new function
                    if condition.test == "EQ" and "Signature" in condition:
                        if "Signature" in condition.left:
                            sig = condition.right.value
                        else:
                            sig = condition.left.value
                        current.tree.right_son.node["name"] = mapping[sig]
                        trees.append(current.tree.right_son)

                    forks.append(Current(
                        tree=current.tree.right_son,
                        root=current.root,
                        state=copy.deepcopy(current.state),
                        block=[],
                        index=index_jump_destination,
                        path=current.path + "1",
                        gas=current.gas,
                        condition=condition,
                        path_constraints=current.path_constraints +
                        [condition],
                        variables=current.variables + condition.get_variables()
                    ))

                    current.tree = current.tree.left_son
                    current.block = []
                    current.path += "0"
                    current.condition = condition.negate()
                    current.path_constraints += [condition.negate()]
                    current.variables += condition.get_variables()

                # in the case where the condition is verified
                elif condition.value:

                    index_jump_destination = [j for j in range(
                        len(opcodes_json)) if opcodes_json[j]["program_counter"] == address_jump.value][0]
                    current.index = index_jump_destination

            # jump instruction
            elif line["name"] == "JUMP":

                address_jump = ret["input"][0]
                index_jump_destination = [j for j in range(
                    len(opcodes_json)) if opcodes_json[j]["program_counter"] == address_jump.value][0]
                current.index = index_jump_destination

    def __bool__(self):
        return bool(self.node or self.left_son or self.right_son)

    def to_json(self, path):
        """
        Saves the tree in json format in <path>

        NOTE: no longer used
        """
        with open(path, "w") as f:
            f.write(MyEncoder().encode(self))

    @classmethod
    def from_json(cls, path):
        """
        Returns a tree from the json file in <path>

        NOTE: no longer used
        """
        with open(path, "r") as f:
            return MyDecoder().decode(f.read())


    def nodes(self):
        """
        Returns the nodes of `self`
        """

        if self:
            if not self.left_son and not self.right_son:
                yield self.node
            elif self.left_son and self.right_son:
                yield self.node
                yield from self.left_son.nodes()
                yield from self.right_son.nodes()
            elif self.left_son:
                yield self.node
                yield from self.left_son.nodes()
            else:
                yield self.node
                yield self.right_son.nodes()

    def get_subtree_from_path(self, path, path_left=None):
        """
        Gets a subtree from his path
        """

        if path_left is None:
            path_left = path[len(self.node["path"]):]

        if path_left == "":
            return self

        elif path_left[0] == "0":
            return self.left_son.get_subtree_from_path(path, path_left[1:])

        else:
            return self.right_son.get_subtree_from_path(path, path_left[1:])

    def find_expr(self, expr, path=None):
        """
        Looks for all the occurences of `expr` in `self`

        Returns a list of couples (`tree`, `line`) where `tree` is a subtree
        which contains `expr` and `line` the row
        of this subtree's block which contains `expr`

        If `path` is given, only searchs `expr` in the way corresponding to `path`

        """

        path_index_list = []

        path_index = 0
        trees_left = deque([self])

        while trees_left:
            current_tree = trees_left.pop()

            # checks if `expr` is in the current tree
            for line in current_tree.node["block"]:
                if expr in line["input"] \
                        or line["output"] and expr in line["output"]:
                    path_index_list.append((current_tree, line))

            # if a path is given
            if path != None and len(path) < path_index:
                if path[path_index] == "0" and current_tree.left_son:
                    trees_left.append(current_tree.left_son)
                elif path[path_index] == "1" and current_tree.right_son:
                    trees_left.append(current_tree.right_son)
                path_index += 1

            # if no path given
            else:
                if current_tree.left_son:
                    trees_left.append(current_tree.left_son)
                if current_tree.right_son:
                    trees_left.append(current_tree.right_son)

        return path_index_list


def main(contract_name, abi):

    print("\n### TREE BUILDER ###\n")

    # loads opcodes
    with open(OPCODES_JSON_PATH, "r") as f:
        json_content = f.read()

    opcodes_json = json.loads(json_content)

    # gets the signatures
    mapping = func_signatures(abi)

    # gets the list of tree of functions
    print("Building the execution trees...")
    tree_functions = BinaryTree.from_opcodes(opcodes_json, mapping)
    print("Building the execution trees : OK\n")

    # # stores trees in json files
    # for t in tree_functions:
    #     t.to_json("../results/{}/json/function{}.json".format(
    #         contract_name, t.node["name"]))
    # print ("{} function trees stored as json in /results/{}/json/\n".format(
    #            len(tree_functions), contract_name))

    return tree_functions
