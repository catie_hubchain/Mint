#
# Display the view of the EVM execution tree
#


import os
import sys
import time
from collections import deque

import numpy as np
import yaml
from PIL import Image, ImageDraw, ImageFont

from sources.tree_builder import BinaryTree

# CONFIG - Loads variables from config.yml
with open("sources/config.yml", "r") as f:
    for k, v in yaml.safe_load(f)["view"].items():
        vars()[k] = v


def cut_string_in(s, n):
    """
    Returns s splitted in n lines
    """
    res = ""
    for i in range(n):
        res += s[(i * len(s)) // n: ((i+1) * len(s)) // n]
        res += "\n"
    res = res[:-1]  # removes the last \n
    return res


def get_resized_line_str(line, drawing):
    """
    Returns `line` potentially on several lines to ensure it not to be wider than the rect  
    """
    pc, name, args = line["program_counter"], line["name"], line["args"]
    if type(args) == int:
        args = hex(args)
    start_line_str = "[%d] %s" % (pc, name)
    start_line_size = drawing.textsize(start_line_str)
    if drawing.textsize(args)[0] <= MAX_RECT_WIDTH - start_line_size[0]:
        return "%s %s\n" % (start_line_str, args)

    elif drawing.textsize("...%s" % args)[0] <= MAX_RECT_WIDTH:
        return "%s...\n...%s\n" % (start_line_str, args)

    else:
        return "%s...\n...%s...\n...%s\n" % (start_line_str, args[:len(args)//2], args[len(args)//2:])


def draw_line(start, tree_topleft_x, tree_topleft_y, tree_height, tree_width, son, offset, condition, drawing):
    """
    Draws a line between two rectangles and display the condition in the middle of it
    """

    if time.time() - start > MAX_TIME:  # stop the drawing if it takes too long
            raise RuntimeError

    if son:

        # displays the line
        topleft_x_son, topleft_y_son = son.node["view"]["rectangle_top_left"] + \
            offset + np.array((-RECT_MARGIN, -RECT_MARGIN))
        height_son = son.node["view"]["rectangle_shape"][1]
        drawing.line(
            (
                (tree_topleft_x + tree_width, tree_topleft_y + tree_height / 2),
                (topleft_x_son, topleft_y_son + height_son / 2)
            ),
            fill=LINE_COLOR
        )

        # displays the condition
        line_width = topleft_x_son - tree_width - tree_topleft_x
        i = 1
        textsize = drawing.textsize(str(condition))
        while textsize[0] > line_width:
            condition = cut_string_in(str(condition), i)
            textsize = drawing.textsize(str(condition))
            i += 1

        middle_x = (tree_topleft_x + tree_width + topleft_x_son) / 2
        middle_y = (tree_topleft_y + tree_height / 2 +
                    topleft_y_son + height_son / 2) / 2
        drawing.text(
            (middle_x - textsize[0] / 2, middle_y - textsize[1] / 2),
            str(condition),
            fill=TEXT_CONDITION_COLOR)


def _update_view_metadata(metadata, view):
    """
    Updates view metadata of tree  
    Used only in add_view_to_tree function  

    `metadata` is supposed to be tree.node["view_metadata"]  
    `view` is supposed to be current["tree"].node["view"]  
    """

    metadata["min_x"] = min(metadata["min_x"], view["rectangle_top_left"][0])

    metadata["max_x"] = max(
        metadata["max_x"], view["rectangle_top_left"][0] + view["rectangle_shape"][0])

    metadata["min_y"] = min(metadata["min_y"], view["rectangle_top_left"][1])

    metadata["max_y"] = max(
        metadata["max_y"], view["rectangle_top_left"][1] + view["rectangle_shape"][1])


def add_height_to_tree(tree, start):
    """
    Adds the depth of each subtrees of the trees to their nodes
    """

    # gets the height of the root
    nodes_left = deque([(tree, 0)])
    root_height = 0

    while nodes_left:

        if time.time() - start > MAX_TIME:  # stop if it takes too long
            raise RuntimeError

        current_tree, current_lvl = nodes_left.pop()
        root_height = max(root_height, current_lvl)
        if current_tree.left_son:
            nodes_left.append((current_tree.left_son, current_lvl + 1))
        if current_tree.right_son:
            nodes_left.append((current_tree.right_son, current_lvl + 1))

    # adds its height to each subtrees
    nodes_left = deque([(tree, root_height)])

    while nodes_left:
        current_tree, current_height = nodes_left.pop()
        current_tree.node["height"] = current_height
        if current_tree.left_son:
            nodes_left.append((current_tree.left_son, current_height - 1))
        if current_tree.right_son:
            nodes_left.append((current_tree.right_son, current_height - 1))


def add_view_to_tree(tree, start):
    """
    Adds attributes to a tree in order to handle its view
    """

    # gets a Draw object to get the size of opcodes representations
    drawing = ImageDraw.Draw(Image.new("RGB", (0, 0)))

    tree.node["view_metadata"] = {
        "min_x": 0,
        "max_x": 0,
        "min_y": 0,
        "max_y": 0
    }

    current = {
        "tree": tree,
        "top_left": np.array((0, 0))
    }

    trees_left = [current]

    while trees_left:

        if time.time() - start > MAX_TIME:  # stop the drawing if it takes too long
            raise RuntimeError

        current = trees_left.pop()

        if current["tree"]:

            current["tree"].node["view"] = {}

            # formates the opcodes to fit to a fine rectangle
            instructions_str = ""
            for i, line in enumerate(current["tree"].node["block"]):
                line_str = get_resized_line_str(line, drawing)
                instructions_str += line_str
            if "returned_value" in current["tree"].node:
                instructions_str += str(current["tree"].node["returned_value"])

            instructions_str += "Gas spent : {}".format(
                    current["tree"].node["gas"])  # add gas at the end
            instructions_str_size = drawing.textsize(instructions_str)

            # gets shape of this rectangle
            current["tree"].node["view"]["rectangle_top_left"] = current["top_left"]
            width = min(instructions_str_size[0],
                        MAX_RECT_WIDTH) + 2 * RECT_MARGIN
            height = instructions_str_size[1] + 2 * RECT_MARGIN

            # fills tree's view attributes
            current["tree"].node["view"]["rectangle_shape"] = np.array(
                (width, height))
            current["tree"].node["view"]["string"] = instructions_str
            current["tree"].node["view"]["condition"] = current["tree"].node["condition"]

            # updates whole_tree_shape for getting window size
            _update_view_metadata(
                tree.node["view_metadata"], current["tree"].node["view"])

            # fills the trees_left stack for next execution of the while loop
            offset = np.array(
                (SPACE_BETWEEN_COLUMNS + MAX_RECT_WIDTH, SPACE_BETWEEN_LINES + height))
            trees_left.append({
                "tree": current["tree"].right_son,
                "top_left": current["top_left"] + offset
            })

            offset[1] *= -1
            trees_left.append({
                "tree": current["tree"].left_son,
                "top_left": current["top_left"] + offset
            })

    # shifts the rectangles if needed
    avoid_collisions(tree, start)


def gets_subtrees_by_height(tree):
    """
    Gets all the subtrees of `tree` for each height
    """

    current_tree = tree

    trees_by_height = {i: [] for i in range(tree.node["height"] + 1)}

    trees_left = [current_tree]

    while trees_left:

        current_tree = trees_left.pop()

        if current_tree:

            trees_by_height[current_tree.node["height"]].append(current_tree)

            trees_left += [current_tree.left_son, current_tree.right_son]

    return trees_by_height


def avoid_collisions(tree, start):
    """
    Shifts some blocks if necessary to avoid two blocks to collide
    """

    d = 1
    # d will be iterated only if there is no collision at height d
    while d < tree.node["height"]:

        if time.time() - start > MAX_TIME:  # stop the drawing if it takes too long
            raise RuntimeError

        no_collision = True

        subtrees_by_height = gets_subtrees_by_height(tree)

        # sorts the subtrees by their height
        for height in subtrees_by_height:
            subtrees_by_height[height].sort(
                key=lambda t: t.node["view"]["rectangle_top_left"][1])

        # compares the trees two by two
        for t1, t2 in zip(subtrees_by_height[d][:-1], subtrees_by_height[d][1:]):

            distance = t2.node["view"]["rectangle_top_left"][1] - \
                t1.node["view"]["rectangle_top_left"][1] - \
                t1.node["view"]["rectangle_shape"][1]

            if distance < 0:
                no_collision = False
                t1.node["view"]["rectangle_top_left"] = (
                    t1.node["view"]["rectangle_top_left"][0],
                    t1.node["view"]["rectangle_top_left"][1] + distance / 2 - 2 * RECT_MARGIN)
                t2.node["view"]["rectangle_top_left"] = (
                    t2.node["view"]["rectangle_top_left"][0],
                    t2.node["view"]["rectangle_top_left"][1] - distance / 2 + 2 * RECT_MARGIN)

        if no_collision:
            d += 1


def get_image_and_drawing(metadata, offset):
    """
    Initiates an image and a drawing in order to draw the tree and returns them  
    This function is only used in draw function  

    `metadata` is supposed to be tree.node["view_metadata"]
    """

    window_width = metadata["max_x"] - metadata["min_x"]
    window_height = metadata["max_y"] - metadata["min_y"]

    # initiates image
    im = Image.new(
        "RGB",
        (window_width + 2 * WINDOW_MARGIN, window_height + 2 * WINDOW_MARGIN),
        BACKGROUND_COLOR)

    # initiates drawing
    drawing = ImageDraw.Draw(im)

    return im, drawing


def draw(tree, start, filepath=""):
    """
    Draws the whole tree on `drawing`  
    If `filepath` is given, stores it in a png file at `filepath`
    """

    # coordonates stored in tree can have negative values
    # so we have to shift the whole tree by some offset
    offset = np.array(
        (WINDOW_MARGIN, abs(tree.node["view_metadata"]["min_y"]) + WINDOW_MARGIN))

    # get the drawing and image
    im, drawing = get_image_and_drawing(tree.node["view_metadata"], offset)

    current = tree

    trees_left = [current]

    while trees_left:

        if time.time() - start > MAX_TIME:  # stop the drawing if it takes too long
            raise RuntimeError

        current = trees_left.pop()

        if current:

            width, height = current.node["view"]["rectangle_shape"]
            topleft_x, topleft_y = current.node["view"]["rectangle_top_left"] + \
                offset + np.array((-RECT_MARGIN, -RECT_MARGIN))

            # draws rectangle
            drawing.rectangle(
                ((topleft_x, topleft_y), (topleft_x + width, topleft_y + height)),
                fill=RECTANGLE_FILLING,
                outline=RECTANGLE_OUTLINE
            )

            # displays opcodes
            drawing.text(
                (topleft_x + RECT_MARGIN, topleft_y + RECT_MARGIN),
                current.node["view"]["string"],
                fill=TEXT_RECT_COLOR
            )

            # draws lines betweens blocks
            for son in (current.left_son, current.right_son):
                if son:
                    draw_line(
                        start, topleft_x, topleft_y, height, width, son, offset,
                        son.node["condition"], drawing
                    )

            # fills trees_left stack for other steps of while loop's execution
            trees_left += [current.left_son, current.right_son]

    if filepath:
        im.save(filepath)
    return im


def main(contract_name, trees):

    print ("\n### VIEW ###\n")

    # # get trees for each functions
    # print("\rLoading the execution trees...")
    # trees = []
    # json_paths = [path for path in os.listdir("../results/json/")
    #               if path.split(".")[-1] == "json" and path[:8] == "function"]

    # for path in json_paths:
    #     trees.append(BinaryTree().from_json("../results/json/%s" % path))
    # print("\rLoading the execution trees : OK\n")

    print("Drawing the trees...")
    nb_trees_drawn = 0
    for i, tree in enumerate(trees):
        if "is_bounded" in tree.node:
            print ("The tree of function {} is too large and will not be displayed.".format(tree.node["name"]))
        elif tree.node["name"] != "fallback":
            start = time.time()
            try:
                add_height_to_tree(tree, start)
                add_view_to_tree(tree, start)
                im = draw(tree, start, "results/{}/img/{}.png".format(contract_name, tree.node["name"]))
                nb_trees_drawn += 1
            except RuntimeError:
                print ("This tree is too large to be drawn, canceling drawing...")
    print("Drawing the trees : OK\n")

    print ("{} trees drew in /results/{}/img/\n".format(nb_trees_drawn, contract_name))
