#
# Classes Data, Memory and Storage
# Classes Concret, Variable, Operation and Test
#


# IMPORTS
import copy
import math
from sources.utils import singleton


class Expression:
    """
    Abstract class for expressions
    """
    expr = []

    def __init__(self):
        self.id = len(self.expr)
        self.expr.append(self)
        


@singleton
class Empty(Expression):
    """
    Represents an empty expression
    """

    __slots__ = ()

    def __eq__(self, obj):
        return isinstance(obj, Empty)

    def __str__(self):
        return ""

    def __contains__(self, var_name):
        return False

    def get_variables(self):
        return []

    def get_expr(self) :
        return []


class Concret(Expression):
    """
    Wraps an integer
    """

    __slots__ = ("value",)

    def __init__(self, value):
        self.value = value
        super().__init__()

    def __eq__(self, obj):
        return isinstance(obj, Concret) and obj.value == self.value

    def __str__(self):
        return str(self.value)

    def __deepcopy__(self, memo):
        return Concret(self.value)

    def __getattr__(self, name):
        if name == "hex":
            return hex(self.value)
        elif name == "dec":
            return self.value
        else:
            return AttributeError(name)

    def __contains__(self, obj):
        return isinstance(obj, int) and obj == self.value

    def get_variables(self):
        return []
    
    def get_expr(self) :
        """
        Returns the list of the expressions in `self`
        """
        return [self]

    def negate(self):
        return Concret(not self.value)


class Variable(Expression):
    """
    class used to represent variables in a symbolic execution
    """

    __slots__ = ("var",)

    def __init__(self, var):
        self.var = var
        super().__init__()

    def __eq__(self, obj):
        return isinstance(obj, Variable) and obj.var == self.var

    def __str__(self):
        return "<%s>" % self.var

    def __contains__(self, obj):
        if isinstance(obj, str) :
            return obj in self.var
        elif isinstance(obj, Variable) :
            return obj == self
        else :
            return False

    def get_variables(self):
        return [self]

    def get_expr(self) :
        """
        Returns the list of the expressions in `self`
        """
        return [self]


class Operation(Expression):
    """
    class used to represent operations in a symbolic execution
    """

    # maps operators symbols with their operators
    OP_EFFECT = {
        "+": lambda x, y: x + y,
        "-": lambda x, y: x - y,
        "x": lambda x, y: x * y,
        "/": lambda x, y: x // y,
        "%": lambda x, y: x % y,
        "^": lambda x, y: x ** y,
        "|": lambda x, y: x | y,
        "&": lambda x, y: x & y,
        # bitwise operator NOT
        "~": lambda x: int("".join(map(lambda c: "0" if c == "1" else "1", format(x, "b"))), 2),
        "XOR": lambda x, y: x ^ y
    }

    __slots__ = ("left", "operator", "right")

    def __init__(self, left=Empty(), operator=None, right=Empty()):
        self.left = left
        self.operator = operator
        self.right = right
        super().__init__()

    def __eq__(self, obj):
        return isinstance(obj, Operation)\
            and self.left == obj.left\
            and self.right == obj.right\
            and self.operator == obj.operator

    def __str__(self):
        if isinstance(self.right, Empty):
            return "%s(%s)" % (self.operator, self.left)
        else:
            return "(%s %s %s)" % (self.left, self.operator, self.right)

    def __contains__(self, obj):
        return obj == self or obj in self.left or obj in self.right

    def get_variables(self):
        return self.left.get_variables() + self.right.get_variables()

    def get_expr(self) :
        """
        Returns the list of the expressions in `self`
        """
        return [self] + self.left.get_expr() + self.right.get_expr()

    def process(self):
        """Computes the operation if possible
        """
        if type(self.left) == type(self.right) == Concret:
            result = self.OP_EFFECT[self.operator](
                self.left.value, self.right.value)
            return Concret(result)
        elif type(self.left) == Concret and type(self.right) == Empty:
            result = self.OP_EFFECT[self.operator](self.left.value)
            return Concret(result)
        else:
            return self


class Test(Expression):
    """
    class used to represent logic tests in a symbolic execution
    """

    # tests and their negations for Test class
    TESTS_NEGATIONS = {
        "LT": "GTE",
        "GT": "LTE",
        "EQ": "NEQ",
        "GTE": "LT",
        "LTE": "GT",
        "NEQ": "EQ"
    }

    # way to print tests
    TESTS_STR = {
        "LT": "<",
        "GT": ">",
        "EQ": "==",
        "GTE": ">=",
        "LTE": "<=",
        "NEQ": "!="
    }

    # maps test symbols to test functions
    TEST_EFFECTS = {
        "LT": lambda x, y: x < y,
        "GT": lambda x, y: x > y,
        "EQ": lambda x, y: x == y,
        "GTE": lambda x, y: x >= y,
        "LTE": lambda x, y: x <= y,
        "NEQ": lambda x, y: x != y
    }

    __slots__ = ("left", "test", "right")

    def __init__(self, left=Empty(), test=None, right=Empty()):
        self.left = left
        self.right = right
        self.test = test
        super().__init__()

    def __eq__(self, obj):
        return isinstance(obj, Test) \
            and self.left == obj.left \
            and self.right == obj.right\
            and self.test == obj.test

    def __str__(self):
        return "(%s %s %s)" % (self.left, self.TESTS_STR[self.test], self.right)

    def __contains__(self, obj):
        return obj == self or obj in self.left or obj in self.right

    def get_variables(self):
        return self.left.get_variables() + self.right.get_variables()

    def negate(self):
        return Test(self.left, self.TESTS_NEGATIONS[self.test], self.right)

    def get_expr(self) :
        """
        Returns the list of the expressions in `self`
        """
        return [self] + self.left.get_expr() + self.right.get_expr()

    def process(self):
        """Computes the test if possible
        """
        if type(self.left) == type(self.right) == Concret:
            result = self.TEST_EFFECTS[self.test](
                self.left.value, self.right.value)
            return Concret(result)
        elif type(self.left) == Concret and type(self.right) == Empty:
            result = self.TEST_EFFECTS[self.test](self.left.value)
            return Concret(result)
        else:
            return self


class Data:
    """Represents data in a contract
    """

    __slots__ = ("holder")

    def __init__(self, *args):
        self.holder = {}
        for i, arg in enumerate(args):
            if type(arg) != Concret:
                arg = Concret(arg)
            self.holder[4 + 32 * i] = arg

    def __getitem__(self, index):
        if isinstance(index, Concret):
            if index.value in self.holder:
                return self.holder[index.value]
            if index.value == 0:
                name = "Signature"
            elif (index.value - 4) % 32 == 0:
                name = "ARG%d" % ((index.value - 4) // 32)
            else:
                name = "DATA%s" % index.value
            self.holder[index.value] = Variable(name)
            return self.holder[index.value]
        else:
            if "expr{}".format(index.id) in self.holder:
                return self.holder["expr{}".format(index.id)]
            self.holder["expr{}".format(index.id)] = Variable(
                "DATA<expr{}>".format(index.id))
            return self.holder["expr{}".format(index.id)]

    def __setitem__(self, index, value):
        if isinstance(index, Concret):
            self.holder[index.value] = value
        else:
            self.holder["expr{}".format(index.id)] = value


class Memory:
    """Represents memory in a contract
    """

    __slots__ = ("holder")

    def __init__(self, **kwargs):
        self.holder = {}
        self.holder.update(kwargs)

    def __getitem__(self, index):
        if isinstance(index, Concret):
            if index.value in self.holder:
                return self.holder[index.value]
            else:
                var = "MEM%d" % index.value
                self.holder[index.value] = Variable(var)
                return self.holder[index.value]
        else:
            if "expr{}".format(index.id) not in self.holder:
                self.holder["expr{}".format(index.id)] = Variable("MEM<expr{}>".format(index.id))
            return self.holder["expr{}".format(index.id)]
        
    def __setitem__(self, index, value):
        if isinstance(index, Concret):
            self.holder[index.value] = value
        else:
            self.holder["expr{}".format(index.id)] = value


class Storage:
    """Represents storage in a contract
    """

    __slots__ = ("holder")

    def __init__(self, **kwargs):
        self.holder = {}
        self.holder.update(kwargs)

    def __getitem__(self, index):
        if isinstance(index, Concret):
            if index.value in self.holder:
                return self.holder[index.value]
            else:
                var = "STORAGE%d" % index.value
                self.holder[index.value] = Variable(var)
                return self.holder[index.value]
        else:
            if "expr{}".format(index.id) not in self.holder:
                self.holder["expr{}".format(index.id)] = Variable("STORAGE<expr{}>".format(index.id))
            return self.holder["expr{}".format(index.id)]
            
    def __setitem__(self, index, value):
        if isinstance(index, Concret):
            self.holder[index.value] = value
        else:
            self.holder["expr{}".format(index.id)] = value
