#
# Argument Parser
# Some decorators used in the program
#

import sys


class ArgData(list):
    """
    Datastruct for storing arguments  
    Used in `ArgParser`
    """

    def __init__(self, *args):
        for arg in args:
            arg = arg.split(" ")
            self.append({
                "little_name": arg[0],
                "big_name": arg[1],
                "nb_inputs": int(arg[2])
            })

    def __getitem__(self, name):
        if name[0:2] == "--":
            arg_name = name[2:]
        elif name[0] == "-":
            arg_name = name[1:]
        else:
            raise Exception("Arguments must begin with '--' or '-'", name)

        for d in self:
            if arg_name == d["little_name"] or arg_name == d["big_name"]:
                return d

        raise KeyError("Argument {} does not exist.".format(name))


class ArgParser:
    """
    Argument Parser
    """

    def __init__(self, *args):
        self.arg_data = ArgData(*args)

    def run(self):
        arg_dict = {}
        i = 1
        while i < len(sys.argv):
            d = self.arg_data[sys.argv[i]]
            inputs = sys.argv[i+1:i+1+d["nb_inputs"]]
            arg_dict[d["big_name"]] = inputs
            i += 1 + d["nb_inputs"]
        return arg_dict


def singleton(cls):
    """
    Decorator for creating singleton classes
    """
    cls._instance_singleton = None

    def new(cl, *args, **kwargs):
        if cl._instance_singleton == None:
            cl._instance_singleton = object.__new__(cl)
        return cl._instance_singleton
    cls.__new__ = new
    return cls


def unique(cls):
    """
    Decorator for creating "unique" classes
    ie object such that if obj1 == obj2 then obj1 is obj2 (in python syntax)  
    `cls` must have a `__unique__` method which take as input arguments of 
    `cls.__init__` and returns an unique identifier for the instance

    eg :  
    @unique  
    class Point :
            def __init__(self, x, y) :
                    self.x, self.y = x, y
            def __unique__(self, x, y) :
                    return (x, y)
            ...

    Then we will have Point(3, 4) is Point(3, 4)
    """
    cls._instances = {}

    def new(cl, *args, **kwargs):
        if cl.__unique__(*args, **kwargs) in cl._instances:
            return cl._instances[cl.__unique__(*args, **kwargs)]
        else:
            instance = object.__new__(cl)
            cl._instances[cl.__unique__(*args, **kwargs)] = instance
            return instance
    cls.__new__ = new
    return cls
