#
# Security checks
#


import json
import os
import sys
from collections import deque

from sources.tree_builder import BinaryTree
from sources.SymbolicClasses import *

LOG_PATH = "results/security.log"

class Logger:
    def __init__(self, *data):
        self.data = list(data)
    
    def log(self, **info):
        self.data.append(info)

    def display_alert(self, code, message, **kwargs):
        print("Security Alert - code {} - {}".format(code, message), **kwargs)

    def alert(self, code, message, **kwargs):
        self.log(code=code, message=message)
        self.display_alert(code, message)

    def store_data(self, path, mode="w"):
        with open(path, mode) as f:
            json.dump(self.data, f, indent=True)


def var_is_safe(var):
    """
    Returns if `var` is safe ie if it cant be modified by users
    """
    safe_variables = ["ADDRESS", "CALLDATASIZE", "CODESIZE", "RETURNDATASIZE",
                    "COINBASE", "TIMESTAMP", "NUMBER", "DIFFICULT", "GASLIMIT",
                    "PC", "MSIZE"]
    if var in safe_variables or "MEM" in var:
        return True
    else:
        return False


def display_and_log_info(code, msg):
    """
    Displays `msg` and stores it in `LOG_PATH`
    """
    print("Security Alert - code {} -".format(code), msg)

    with open(LOG_PATH, "r") as f:
        content = f.read()
        if content:
            logs = json.loads(content)
        else:
            logs = []
    
    logs.append( 
        {
            "code": code, 
            "message": msg
        })

    with open(LOG_PATH, "a") as f:
        json.dump(logs, f, indent=True)        
        f.write("\n")


def function_is_suicidable(function_tree):
    """
    Returns if a function is suicidable or not
    ie if it can be killed without restriction
    """

    for node in function_tree.nodes():

        if "SUICIDE" in [line["name"] for line in node["block"]]:
            is_suicidable = True
            for ctr in node["path_constraints"]:
                if "CALLER" in ctr and "STORAGE" in ctr:
                    is_suicidable = False
                    break
            if is_suicidable:
                return True

    return False


def function_is_prodigal(function_tree):
    """
    Returns if a function is prodigal or not
    ie if it can send ether without restriction
    """

    for node in function_tree.nodes():
        callers = []
        calls = [line for line in node["block"] if line["name"] == "CALL"]
        
        # gets the target address of CALL instruction
        for call in calls :
            value = call["input"][2]
            flag = False
            for ctr in node["path_constraints"]:
                if Test(Concret(0), "EQ", value) in ctr\
                        or Test(value, "EQ", Concret(0)) in ctr: #value != 0
                    flag = True
                    break
            if not flag:
                callers.append(call["input"][1])

        # checks if there is some test on these addresses
        # or if the msg.sender's identity is checked
        for caller in callers :
            check_on_caller = False
            for ctr in node["path_constraints"] :
                if caller in ctr or "CALLER" in ctr:
                    check_on_caller = True
            if not check_on_caller:
                return True
        
    return False


def function_is_payable(function_tree):
    """
    Returns if a function can receive ether or not
    """

    for node in function_tree.nodes():

        if "REVERT" not in [line["name"] for line in node["block"]]:

            flag = False
            for ctr in node["path_constraints"]:
                if Test(Variable("VALUE"), "EQ", Concret(0)) in ctr\
                    or Test(Concret(0), "EQ", Variable("VALUE")) in ctr:
                        flag = True
            if not flag:
                return True

    return False


def function_can_send_ether(function_tree):
    """
    Returns if a function can send ether or not
    """

    for node in function_tree.nodes():

        # if the function can be killed
        ins = [line["name"] for line in node["block"]]
        if "SELFDESTRUCT" in ins or "SUICIDE" in ins:
            return True

        # if the function can send ether with CALL instruction
        calls = [line for line in node["block"] if line["name"] == "CALL"]
        for call in calls:
            value = call["input"][2]
            flag = False
            for ctr in node["path_constraints"]:
                if Test(Concret(0), "EQ", value) in ctr\
                        or Test(value, "EQ", Concret(0)) in ctr: #value != 0
                    flag = True
            if not flag:
                return True

    return False


def check_on_call(function_tree):
    """
    Returns if there is a check on call return value
    """

    for node in function_tree.nodes():

        call_ret = None
        check = False
        for line in node["block"]:
            if line["name"] == "CALL":
                call_ret = line["output"]  # value returned by CALL instruction
            elif line["name"] == "JUMPI" and call_ret:

                # returns False if the test following the CALL 
                # is not `ISZERO(call_ret)`
                test = line["input"][1]
                if test.test != "EQ"\
                        or Concret(0) not in (test.left, test.right)\
                        or call_ret not in test:
                    return False
                else:
                    check = True
                    break
    return True if not call_ret or check else False


def call_arbitrary_address(function_tree):
    """
    Returns if `function_tree` can call external contracts without verification.

    Checks if the function can call an address given by a user.
    If it does, checks if there are tests involving
    this(these) address(es).

    """

    for node in function_tree.nodes():
        called = []
        calls = [line for line in node["block"] if line["name"] == "CALL"]

        # gets the unsafe CALL instruction's target addresses
        # also test the gas provided : if it is fixed then it is ok
        for line in calls:
            var = line["input"][1]
            gas = line["input"][0]
            if not var_is_safe(var) and gas == Variable("GAS"):
                called.append(var)

        # checks if these elements are tested or not
        # or if the sender's identity is checked
        for c in called:
            check_on_caller = False
            for ctr in node["path_constraints"]:
                if c in ctr or "CALLER" in ctr:
                    check_on_caller = True
            if not check_on_caller:
                return True

    return False


def too_much_gas_on_fallback(fallback_tree) :
    """
    Returns if fallback can consume more than 2300 gas
    """
    gas = 0
    for node in fallback_tree.nodes():
        gas = max(gas, node["gas"])
    if gas > 2300 :
        return gas
    else:
        return False


def operations_overflow_underflow(function_tree):
    """
    Returns the list of the operations that may be attack by underflow/overflow
    """
    vulnerables = []

    for node in function_tree.nodes():
        
        # Gets the + - x ^ operations
        ops = [line["output"] for line in node["block"]\
                 if isinstance(line["output"], Operation)\
                 and line["output"].operator in ("+", "-", "x", "^")]
        
        # Gets unsafe variables in ops
        for op in ops:
            for var in op.get_variables():
                if not var_is_safe(var):
                    vulnerables.append(op)
    
    return vulnerables


def overflow_underflow(function_tree):
    """
    Returns if a function is vulnerable to an overflow/underflow attack
    """
    vulnerable_tests = []
    vulnerables_ops = operations_overflow_underflow(function_tree)
    
    for node in function_tree.nodes():

        # get JUMPI instructions
        jumpif = [line for line in node["block"] if line["name"]=="JUMPI"]
        
        # Checks if a test is made of vulnerable operations
        for test in [jmp["input"][1] for jmp in jumpif]:
            for vuln_op in vulnerables_ops:
                if vuln_op in test:
                    return True

    return False


def main(contract_name, trees):

    print("\n### SECURITY ###\n")
    nb_alerts = 0

    logger = Logger()

    # Checks if the contract is suicidable
    for i, tree in enumerate(trees):
        if function_is_suicidable(tree):
            logger.alert(
                1, 
                "Contract {} could be suicidable ! (function {})".format(
                    contract_name, tree.node["name"]))
            
            nb_alerts += 1

    # Checks if the contract is prodigal
    for i, tree in enumerate(trees):
        if function_is_prodigal(tree):
            logger.alert(
                2,
                "Contract {} could be prodigal ! "
                "(function {})".format(contract_name, tree.node["name"]))
            nb_alerts += 1

    # Checks if at least one function is payable
    is_payable = False
    for tree in trees:
        if function_is_payable(tree):
            is_payable = True
            break

    # Checks if at least one function can send ether
    if is_payable:
        can_send_ether = False
        for tree in trees:
            if function_can_send_ether(tree):
                can_send_ether = True
                break
        if not can_send_ether:
            logger.alert(
                3,
                "Contract {} could lock ether !"
                " (function {})".format(contract_name, tree.node["name"]))
            nb_alerts += 1

    # Checks if there is checks on CALL return values
    for i, tree in enumerate(trees):
        if not check_on_call(tree):
            logger.alert(
                4,
                "Contract {} - Missing check on CALL"
                " instruction ! (function {})".format(
                    contract_name, tree.node["name"]))
            nb_alerts += 1

    # Checks if there are calls to arbitrary addresses
    for i, tree in enumerate(trees):
        if call_arbitrary_address(tree):
            logger.alert(
                5,
                "Contract {} - function {} can call arbitrary"
                " addresses".format(
                    contract_name, tree.node["name"]))
            nb_alerts += 1

    # Checks if fallback needs to much gas
    fallback = [tree for tree in trees if tree.node["name"] == "fallback"][0]
    gas = too_much_gas_on_fallback(fallback)
    if gas:
        logger.alert(
            6,
            "Contract {} - function fallback needs more "
            "than 2300 gas (it needs {} gas), this could lead to a failure in "
            "sendings or callings.".format(contract_name, gas))
        nb_alerts += 1

    # Checks on overflow underflow faults
    for tree in trees:
        if overflow_underflow(tree):
            logger.alert(
                7,
                "Contract {} - function {} is vulnerable to overflow/underflow"
                " tests.".format(contract_name, tree.node["name"]))
            nb_alerts += 1

    # Sums up
    logger.store_data(LOG_PATH)

    print("{} alert(s) found in contract {}".format(
        nb_alerts, contract_name), end="")
    if nb_alerts:
        print(", see ./results/security.log for more information.")
    else:
        print(".")

    return nb_alerts
