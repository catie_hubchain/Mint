#
# Translate EVM bytecode to opcodes
#


# IMPORTS
import json
import os

import yaml  # for config file


# CONFIG - Loads variables from config.yml
with open("sources/config.yml", "r") as f:
    for k, v in yaml.safe_load(f)["translater"].items():
        vars()[k] = v


def translate_bytecode_to_opcode(bytecode):
    """
    Creates json and txt file of opcodes corresponding to the bytecode given in input
    
    NOTE: json file is no longer used
    """

    # gets bytecodes-opcodes translation
    with open(BYTECODE_TRANSLATION_PATH, "r") as f:
        bytecodes_translation = json.load(f)

    bytecode = bytecode.upper()
    opcode_readable = ""  # the future txt file content
    opcode_dicts_list = []  # the future json file content
    program_counter = -1  # program counter for the execution
    nb_args = 0

    for byte in [bytecode[2 * i:  2 * i + 2] for i in range(len(bytecode)//2)]:
        # Manages arguments
        if nb_args:
            opcode_readable += byte
            opcode_dicts_list[-1]["args"] += str(byte)
            nb_args -= 1
            if not nb_args:
                opcode_readable += "\n"
            continue

        # Manages unknown bytes
        if not byte in bytecodes_translation:
            program_counter += 1
            opcode_readable += "[{}] (Unknown Opcode) ({})\n".format(
                program_counter,
                byte
            )
            opcode_dicts_list.append({
                "program_counter": program_counter,
                "name": "UnknownOpcode",
                "args": "",
                "gas": 0})

        else:
            name = bytecodes_translation[byte]["opcode"]

            if name[0:4] != "PUSH":
                name = bytecodes_translation[byte]["opcode"]
                gas = bytecodes_translation[byte]["gas"]
                program_counter += 1
                opcode_readable += "[%d] " % program_counter + name + "\n"
                opcode_dicts_list.append({
                    "program_counter": program_counter,
                    "name": name,
                    "args": "",
                    "gas": gas})

            else:
                # Manages arguments
                nb_args = int(name[4:])
                gas = bytecodes_translation[byte]["gas"]
                program_counter += 1 + nb_args
                opcode_readable += "[{}] PUSH{} 0x".format(
                        program_counter, nb_args)
                opcode_dicts_list.append({
                    "program_counter": program_counter,
                    "name": name,
                    "args": "",
                    "gas": gas})

    return opcode_readable, opcode_dicts_list


def main(bytecode=None) :

    print ("\n### TRANSLATER ###")

    if not bytecode :
        bytecode = input("Please, give a bytecode :\n")

    if bytecode[:2] == "0x" :
        bytecode = bytecode[2:]

    opcode_readable, opcode_dicts_list = translate_bytecode_to_opcode(bytecode)

    # creates txt file
    with open(OPCODE_READABLE_PATH, "w") as f:
        f.write(opcode_readable)
        print ("Version readable of opcodes written in %s" %
               OPCODE_READABLE_PATH)

    # creates json file
    for d in opcode_dicts_list:
        if d["args"]:
            # turns all the str arguments to int
            d["args"] = int("0x" + d["args"], 16)

    with open(OPCODE_JSON_PATH, "w") as f:
        # json.dump({op["program_counter"] : op for op in opcode_dicts_list}, f)
        f.write(json.dumps(opcode_dicts_list))
        print ("Json file of opcodes written in %s\n" % OPCODE_JSON_PATH)
