#
# Emulate the EVM, handle symbolic execution
#





#IMPORTS
from collections import deque
from sources.SymbolicClasses import *





class State :
    """
    Representation of the EVM (essentially the stack, data, memory and storage)
    """

    __slots__ = ("stack", "data", "mem", "storage")

    def __init__(self, stack=None, data=None, mem=None, storage=None, **kwargs) :
        self.stack = stack if stack else deque()
        self.data = data if data else Data()
        self.mem = mem if mem else Memory()
        self.storage = storage if storage else Storage()
        for k, v in kwargs.items() :
            setattr(self, k, v)


    def STOP(self) :
        return {"input" : (), "output" : None}


    def ADD(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "+", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def MUL(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "x", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}



    def SUB(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "-", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
        


    def DIV(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "/", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
    

    def SDIV(self) :
        return self.DIV()
    

    def MOD(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "%", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def SMOD(self) :
        return self.MOD()
    

    def ADDMOD(self) :
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        output = Operation(Operation(a, "+", b).process(), "%", c).process()
        self.stack.append(output)
        return {"input" : (a, b, c), "output" : output}


    def MULMOD(self) :
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        output = Operation(Operation(a, "x", b).process(), "x", c).process()
        self.stack.append(output)
        return {"input" : (a, b, c), "output" : output}


    def EXP(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "^", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}

    

    def LT(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Test(a, "LT", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def GT(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Test(a, "GT", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def SLT(self) :
        return self.LT()


    def SGT(self) :
        return self.GT()


    def EQ(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Test(a, "EQ", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def ISZERO(self) :
        a = self.stack.pop()
        output = Test(a, "EQ", Concret(0)).process()
        self.stack.append(output)
        return {"input" : (a,), "output" : output}


    def AND(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "&", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
    

    def OR(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "|", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
    
    
    def XOR(self) :
        a,b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "XOR", b).process()
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
        

    def NOT(self) :
        a = self.stack.pop()
        output = Operation(a, "~", Empty()).process()
        self.stack.append(output)
        return {"input" : (a,), "output" : output}
    

    def BYTE(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "BYTE", Empty())
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}
    

    def SHA3(self) :
        a, b = self.stack.pop(), self.stack.pop()
        output = Operation(a, "SHA3", Empty())
        self.stack.append(output)
        return {"input" : (a, b), "output" : output}


    def ADDRESS(self) :
        output = Variable("ADDRESS")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def BALANCE(self) :
        a = self.stack.pop()
        output = Variable("BALANCE_%s"%a)
        self.stack.append(output)
        return {"input" : (a,), "output" : output}


    def ORIGIN(self) :
        output = Variable("ORIGIN")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def CALLER(self) :
        output = Variable("CALLER")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def CALLVALUE(self) :
        output = Variable("VALUE")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def CALLDATALOAD(self) :
        a = self.stack.pop()
        output = self.data[a]
        self.stack.append(output)
        return {"input" : (a,), "output" : output}


    def CALLDATASIZE(self) :
        output = Variable("DATASIZE")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def CALLDATACOPY(self) :
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        
        if c == 32 :
            mem_data = self.data[b]
            self.mem[c] = mem_data

        elif type(self.data[b]) == Variable :
            mem_data = Variable(self.data[b].var + "[: %sB]" %c)
            self.mem[c] = mem_data
        
        elif type(self.data[b]) == Concret :
            mem_data = Concret(self.data[b].value // 16**(32-c))
            self.mem[c] = mem_data
        
        return {"input" : (a, b, c), "output" : None, "mem" : (c, mem_data)}
        

    def CODESIZE(self) :
        output = Variable("CODESIZE")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def CODECOPY(self) : #Not implemented yet
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        return {"input" : (a, b, c), "output" : None}
    

    def GASPRICE(self) :
        output = Variable("GASPRICE")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def EXTCODESIZE(self) :
        a = self.stack.pop()
        output = Variable("CODESIZE_OF_%s"%a)
        self.stack.append(output)
        return {"input" : (a,), "output" : output}
    

    def EXTCODECOPY(self) :
        a, b, c, d = self.stack.pop(), self.stack.pop(), self.stack.pop(), self.stack.pop()
        return {"input" : (a, b, c, d), "output" : output}
        

    def RETURNDATASIZE(self) :
        output = Variable("DATASIZE")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def RETURNDATACOPY(self) : #WIP
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        return {"input" : (a, b, c), "output" : None}
        

    def BLOCKHASH(self) :
        a = self.stack.pop()
        output = Variable("HASH_BLOCK%s"%a)
        self.stack.append(output)
        return {"input" : (a,), "output" : output}
    

    def COINBASE(self) :
        output = Variable("COINBASE")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def TIMESTAMP(self) :
        output = Variable("TIMESTAMP")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def NUMBER(self) :
        output = Variable("NUMBER")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def DIFFICULTY(self) :
        output = Variable("DIFFICULTY")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def GASLIMIT(self) :
        output = Variable("GASLIMIT")
        self.stack.append(output)
        return {"input" : (), "output" : output}


    def POP(self) :
        self.stack.pop()
        return {"input" : (), "output" : None}
        

    def MLOAD(self) :
        a = self.stack.pop()
        output = self.mem[a]
        self.stack.append(output)
        return {"input" : (a,), "output" : output}


    def MSTORE(self) :
        a, b = self.stack.pop(), self.stack.pop()
        self.mem[a] = b
        return {"input" : (a, b), "output" : None, "mem" : (a, b)}
    

    def MSTORE8(self) :
        return self.MSTORE()


    def SLOAD(self) :
        a = self.stack.pop()
        output = self.storage[a]
        self.stack.append(output)
        return {"input" : (a,), "output" : output}


    def SSTORE(self) :
        a, b = self.stack.pop(), self.stack.pop()
        self.storage[a] = b
        return {"input" : (a, b), "output" : None, "storage" : (a, b)}


    def JUMP(self) :
        a = self.stack.pop()
        return {"input" : (a,), "output" : None}


    def JUMPI(self) :
        address, constraint = self.stack.pop(), self.stack.pop()
        return {"input" : (address, constraint), "output" : None}


    def PC(self) :
        output = Variable("PC")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    
    
    def MSIZE(self) :
        output = Variable("MSIZE")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def GAS(self) :
        output = Variable("GAS")
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def JUMPDEST(self) :
        return {"input" : (), "output" : None}


    def PUSH(self, _bytes) :
        if type(_bytes) == int :
            output = Concret(_bytes)
        else :
            output = Variable(_byte)

        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def DUP(self, offset) :
        output = self.stack[-offset]
        self.stack.append(output)
        return {"input" : (), "output" : output}
    

    def SWAP(self, offset) :
        tmp = self.stack[-offset-1]
        self.stack[-offset-1] = self.stack[-1]
        self.stack[-1] = tmp
        return {"input" : (), "output" : None}
    

    def LOG(self, x) :
        args = tuple(self.stack.pop() for _ in range(x+2))
        return {"input" : args, "output" : None}


    def CREATE(self) :
        a, b, c = self.stack.pop(), self.stack.pop(), self.stack.pop()
        output = Variable("NEW_CONTRACT_ADDRESS")
        self.stack.append(output)
        return {"input" : (a, b, c), "output" : output}


    def CALL(self) :
        args = tuple(self.stack.pop() for _ in range(7))
        output = Variable("CALL_RETURNED_STATUS")
        self.stack.append(output)
        return {"input" : args, "output" : output}
    

    def CALLCODE(self) :
        args = tuple(self.stack.pop() for _ in range(7))
        output = Variable("CALLCODE_RETURNED_STATUS")
        self.stack.append(output)
        return {"input" : args, "output" : output}
    

    def RETURN(self) :
        args = tuple(self.stack.pop() for _ in range(2))
        returned_value = self.mem[args[0]]
        return {"input" : args, "output" : None, "returned" : returned_value}


    def DELEGATECALL(self) :
        args = tuple(self.stack.pop() for _ in range(6))
        output = Variable("DELEGATECALL_RETURNED_STATUS")
        self.stack.append(output)
        return {"input" : args, "output" : output}
        

    def STATICCALL(self) :
        args = tuple(self.stack.pop() for _ in range(6))
        output = Variable("STATICCALL_RETURNED_STATUS")
        self.stack.append(output)
        return {"input" : args, "output" : output}
    

    def REVERT(self) :
        args = tuple(self.stack.pop() for _ in range(2))
        return {"input" : args, "output" : None}


    def INVALIDOPERATION(self) :
        return {"input" : (), "output" : None}


    def SELFDESTRUCT(self) :
        a = self.stack.pop()
        return {"input" : (a,), "output" : None}

    
    def SUICIDE(self) :
        return self.SELFDESTRUCT()


    def UnknownOpcode(self) :
        return {"input" : (), "output" : None}





    def next_line(self, line) :
        """
        Calls the method given by <line>
        """
        opcode = line["name"]
        
        if opcode[:4] == "PUSH" :
            args = line["args"]
            return self.PUSH(args)

        elif opcode[:4] == "SWAP" :
            args = opcode[4:]
            return self.SWAP(int(args))
        
        elif opcode[:3] == "DUP" or opcode[:3] == "LOG" :
            func = getattr(self, opcode[:3])
            args = opcode[3:]
            return func(int(args))

        else :
            func = getattr(self, opcode)
            return func()